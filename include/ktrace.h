/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __KTRACE__H
#define __KTRACE__H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <tracefs.h>
#include <ccli.h>

#define ARRAY_SIZE(array) (sizeof(array) / sizeof((array)[0]))

struct ktrace_commands {
	const char		*command;
	const char		*usage;
	const char		*help;
	struct ktrace_commands	*link;
};

int ktrace_help(struct ccli *ccli, const char *arg1, const char *arg2);

int help_completion(struct ccli *ccli, const char *command,
		    const char *line, int word,
		    char *match, char ***list, void *data);
int cmd_help(struct ccli *ccli, const char *command, const char *line,
	     void *data, int argc, char **argv);

int create_completion(struct ccli *ccli, const char *command,
		      const char *line, int word,
		      char *match, char ***list, void *data);
int cmd_create(struct ccli *ccli, const char *command, const char *line,
	       void *data, int argc, char **argv);

int enable_completion(struct ccli *ccli, const char *command,
		      const char *line, int word,
		      char *match, char ***list, void *data);
int cmd_enable(struct ccli *ccli, const char *command, const char *line,
	       void *data, int argc, char **argv);

int disable_completion(struct ccli *ccli, const char *command,
		       const char *line, int word,
		       char *match, char ***list, void *data);
int cmd_disable(struct ccli *ccli, const char *command, const char *line,
		void *data, int argc, char **argv);

struct tep_event *find_event(struct tep_handle *tep, char *ename);

int event_completion(struct ccli *ccli, struct tep_handle *tep,
		     char ***list, int *cnt, char *match, char append);

#endif
